//Sekwayi Mokoena
//module_2_app3

void main() {
  
  var appWinner = AppWinners();
  
  print('The following app is the 2021 winner');
  print('Name: ${appWinner.appName}');
  print('Developer: ${appWinner.appDeveloper}');
  print('Category: ${appWinner.appCategory}');
  print(appWinner.transformName(appWinner.appName));
}

class AppWinners {
  
  var appName = 'Ambani Africa';
  var appDeveloper = 'Mukunda Lambani';
  var appCategory = 'Education';
  var appYear = '2021';
  
  String transformName(var name){
    return appName.toUpperCase();
  }
  
}