//Sekwayi Mokoena
//module_2_app1

void main() {
  String name = 'Sekwayi Mokoena';
  String favApp = 'Brave Browser';
  String city = 'Johannesburg';

  print('$name mentioned that his/her favorite app is $favApp,');
  print('and he/she lives in $city!');
}